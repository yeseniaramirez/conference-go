from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_picture(city):
    url = "https://api.pexels.com/v1/search"
    headers = {"Authorization": PEXELS_API_KEY}
    payload = {"query": city}
    response = requests.get(url, headers=headers, params=payload)

    picture = json.loads(response.content)

    photos = picture["photos"]

    return photos


def get_weather_data(city, state):
    url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},&limit=5&appid={OPEN_WEATHER_API_KEY}"

    response = requests.get(url)

    data = json.loads(response.content)[0]
    lat = data["lat"]
    lon = data["lon"]

    # lat: 30.2711286
    # lon: -97.7436995

    weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"

    weather_response = requests.get(weather_url)
    weather_data = json.loads(weather_response.content)

    description = weather_data["weather"][0]["description"]
    temperature = weather_data["main"]["temp"]

    weather = {"temp": temperature, "description": description}

    return weather
